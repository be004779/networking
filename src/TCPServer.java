import java.io.*;
import java.net.*;


public class TCPServer{


    private ServerSocket server;
    private String serverIP;
    @SuppressWarnings("unused")
    private int serverPort;

/*
Defining a constructor to create a new server with a new port and IP address
 */
    public TCPServer(String ipAddress, int port) throws Exception {
        this.serverPort = port;
        this.serverIP = ipAddress;

        if (ipAddress != null && !ipAddress.isEmpty()) { //if statement to check that the IP address value is not null in order to create a new server.
            this.server = new ServerSocket(port, 1, InetAddress.getByName(serverIP));
        } else {
            this.server = new ServerSocket(0, 1, InetAddress.getLocalHost()); //else statement makes sure that if no IP address is defined, the local host is taken as the IP.
        }
    }

    /*
    listen function makes sure that the messages from client are received and printed to the server terminal as well
    as sending messages to the client side. Both done through the socket.
     */

    public void listen() throws Exception {
        Socket s = server.accept(); // accepting the client connection coming through the socket. Defined and initialised.
        String[] input = {"Welcome to reading.ac.uk.", "Please enter sender's email address: ", "Please enter recipient's email address: "}; //string array of messages that will be printed to the client side
        String data = null; // string used to take in messages through the buffered reader from the client side.
        PrintWriter output = new PrintWriter(s.getOutputStream()); //print writer to get the messages from server that are being printed on client side.
        InputStreamReader in = new InputStreamReader(s.getInputStream()); // input stream reader and buffered reader to receive messages from client and to be converted to string to be printed to server terminal.
        BufferedReader bf = new BufferedReader(in);
        System.out.println("\r\nNew client connection from " + serverIP);


        output.println(input[0]); //printing out the different strings in the array on client side
        output.flush();
        output.println(input[1]); //each string is printed after the client input is taken in, as a sequence.
        output.flush();
        output.println(input[2]);
        output.flush();


        try (FileWriter fw = new FileWriter("output.txt")) { //file writer is created to write the user input to a file.
            while (true) {
                if (data != "exit") {
                    data = bf.readLine(); //buffered reader reads the message and converts to string and stored in data variable.
                    System.out.println("\r\nMessage from Client: " + data); //printing out the client's message on the server terminal
                    fw.write(data); //writing the client's messages to a text file
                    fw.flush(); //flushes the message
                } else {
                    break;
                }
            }
            fw.close(); //important to close the file writer at the end of the while loop to close the process.

        } catch (Exception exception){
            System.out.println("The client has left: " + exception); //exception for when the client disconnects
        }
        output.close(); // again important to close the print writer and buffered reader at the end of the function.
        bf.close();
    }

/*
Main function to initialise the listen method and to also define the IP, port and to create a new server with the constructor.
 */
    public static void main(String[] args) throws Exception {
        String serverIP = "127.0.0.1";
        int serverPort = 4999;
        TCPServer server = new TCPServer(serverIP, serverPort); //creating a new server with the specified ip and port.

        System.out.println("Server has started on port: " + serverPort);
        server.listen(); //initialising the listen function within main
    }
}
