import java.io.*;
import java.net.*;
import java.util.Scanner;


public class TCPClient{
    private Socket s;
    private Scanner scanner;

    /*
    creating a constructor to initialise a new client with the scanner, IP and port variables that will be later defined
     */

    public TCPClient(String serverAddress, int serverPort) throws Exception {

        this.s = new Socket(serverAddress, serverPort);
        scanner = new Scanner(System.in); //new scanner to take user input
    }

    /*
    listen function like in server, to take in messages coming from server and print them, to send messages through the
    scanner using the print writer and also the buffered reader to receive the messages.
     */
    public void listen() throws Exception {

        String data = null;
        String input; // string this time is not an array as user input will be taken and stored here
        PrintWriter output = new PrintWriter(s.getOutputStream());; //print writer to send messages across the socket
        System.out.println("\r\nConnected to Server");
        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream())); //buffered reader with the input stream reader defined inside to receive and read messages from the server side

        while(true) { //while loop to take in user input then push this message to the server side and waiting for the server messages to come through
            input = scanner.nextLine(); //user input
            if(input != "exit") {
                output.println(input); // pushing the message through socket and flushing
                output.flush();
            }else {
                break;
            }
            data = in.readLine(); //reading server messages and printing to server terminal in the print statement
            System.out.println("\r\nMessage from Server: " + data);
        }
        output.close(); //closing the print writer to end the process
    }

    /*
    Main function to define the port, IP and to create a new client using the constructor and initialising the listen function.
     */

    public static void main(String[] args) throws Exception {
        String clientIP = "127.0.0.1";
        int clientPort = 4999;
        TCPClient client = new TCPClient(clientIP, clientPort); //creating a new client with specified IP and port
        client.listen(); //initialising listen function
    }
}